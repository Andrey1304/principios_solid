/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Negocio.Division;
import Negocio.Multiplicacion;
import Negocio.Operacion;
import Negocio.Resta;
import Negocio.Suma;
import java.util.Scanner;



/**
 *
 * @author Andrey R
 */
public class Main extends Operacion{
    public static void main(String[] args){
      
        double n1 =50;
        /*System.out.println ("Por favor introduzca el primer numero");
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        n1 = Double.parseDouble(entradaEscaner.nextLine()); //Invocamos un método sobre un objeto Scanner*/ 
        double n2 = 5;  
        /*System.out.println ("Por favor introduzca el primer numero");
        Scanner entradaEscaner2 = new Scanner (System.in); //Creación de un objeto Scanner
        n2 = Double.parseDouble(entradaEscaner2.nextLine()); //Invocamos un método sobre un objeto Scanner*/
       
    //suma   
    Suma sum = new Suma(n1,n2);
    sum.mostrarResultado();
        
    //resta
    Resta res = new Resta(n1,n2);
    res.mostrarResultado();
        
    //multiplicacion
    Multiplicacion mul = new Multiplicacion(n1,n2);
    mul.mostrarResultado();
        
    //division
    Division div = new Division(n1,n2);
    div.mostrarResultado();
        
    }
}
